{{-- This is a temporary file just to create the way to do questions --}}
@extends('layouts.app')

@section('content')

<question-types></question-types>
<activation-sections 
    :activation="{{ json_encode($companyActivation) }}">
</activation-sections>

@endsection

@section('page_js')
<script>
    _URL.set('question-type.index', '{{ route('question-type.index') }}');
</script>
<script src="{{ mix('js/questionary.js') }}"></script>
@endsection