window.Vue = require('vue');
import General from '../libs/general';
import { computed } from '../libs/globalMixins';
import store from "../store/store";

Vue.mixin({ computed });
require('../bootstrap');

Vue.component('question-types', require('../components/questionary/QuestionTypes.vue').default);
Vue.component('activation-sections', require('../components/questionary/ActivationSections.vue').default);
Vue.component('activation-questions', require('../components/questionary/ActivationQuestions.vue').default);
Vue.component('toast', require('../components/general/Toast.vue').default);

new Vue({
    el: '#app',
    store,
    mounted() {
        new General;
    }
});
