<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <toast></toast>
                    @yield('content')
                </div>
            </div>
        </div> <!-- container-fluid -->
        
    </div> <!-- content -->
    
    <footer class="footer">
        2019 © Atelier. <span class="d-none d-sm-inline-block"> - Daniel Flórez Murillo</span>
    </footer>
    
    
</div>

<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->