<?php

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::get('login-gate/{token}', 'Auth\LoginController@loginGate');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
// Authentication Routes...
Route::middleware(['auth'])->group(function(){
    Route::get('/', 'HomeController@index')->name('index');
    Route::resource('activation', 'ActivationController');
    Route::apiResource('user', 'UserController');
    Route::apiResource('activation-question', 'ActivationQuestionController');

    //Aditional services
    Route::get('company-activations', 'ActivationController@apiGetData')->name('activations.company.get');
    Route::get('users-get', 'UserController@GetUsers')->name('users.get');
    Route::get('roles-get', 'UserController@GetRoles')->name('roles.get');
    Route::get('companies-get', 'CompanyController@GetCompanies')->name('companies.get');
    Route::get('question-type', 'QuestionTypeController@index')->name('question-type.index');
});

Route::get('test-dfm', function() {

    $activationSection = [
        'company_activation_id' => 1,
        'title' => 'Mi first activation',
        'page' => 1,
    ];
    \App\ActivationSection::create($activationSection);

    $questions = [
        [
            'activation_section_id' => 1,
            'question_type_id' => 1,
            'question' => 'This is my question',
            'options' => null,
            'order' => 1,
            'caption' => 'my caption',
            'required' => true,
        ], [
            'activation_section_id' => 1,
            'question_type_id' => 2,
            'question' => 'This is my question 2',
            'options' => json_encode(['val1'=>'opc1', 'val2'=>'opc2','val3'=>'opc3','val4'=>'opc4','val5'=>'opc5','val6'=>'opc6',]),
            'order' => 2,
            'caption' => 'my caption 2',
            'required' => false,
        ], [
            'activation_section_id' => 1,
            'question_type_id' => 3,
            'question' => 'This is my question 3',
            'options' => json_encode(['val1'=>'opc1', 'val2'=>'opc2','val3'=>'opc3','val4'=>'opc4','val5'=>'opc5','val6'=>'opc6',]),
            'order' => 3,
            'required' => false,
        ], [
            'activation_section_id' => 1,
            'question_type_id' => 4,
            'question' => 'This is my question 4',
            'options' => json_encode(['val1'=>'opc1', 'val2'=>'opc2','val3'=>'opc3','val4'=>'opc4','val5'=>'opc5','val6'=>'opc6',]),
            'order' => 4,
            'required' => false,
        ], [
            'activation_section_id' => 1,
            'question_type_id' => 5,
            'question' => 'This is my question 5',
            'order' => 5,
            'restrictions' => json_encode(['min'=>10, 'max'=>20]),
            'required' => false,
        ], [
            'activation_section_id' => 1,
            'question_type_id' => 6,
            'question' => 'This is my question 6',
            'order' => 6,
            'required' => false,
        ], [
            'activation_section_id' => 1,
            'question_type_id' => 7,
            'question' => 'This is my question 7',
            'order' => 7,
            'restrictions' => json_encode(['min'=>'today', 'max'=>'+;20;d']),
            'required' => false,
        ], [
            'activation_section_id' => 1,
            'question_type_id' => 8,
            'question' => 'This is my question 8',
            'order' => 8,
            'required' => false,
        ], [
            'activation_section_id' => 1,
            'question_type_id' => 9,
            'question' => 'This is my question 9',
            'order' => 9,
            'required' => false,
        ]
    ];

    foreach ($questions as $question) {   
        \App\ActivationQuestion::create($question);
    }

});