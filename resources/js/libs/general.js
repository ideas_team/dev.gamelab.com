class General {

    constructor(){
        this.init();
    }

    init(){
        this._InitMenuMobile();
    }

    _InitMenuMobile(){
        document.getElementById('mobile-menu-button').onclick = (e) => {
            e.preventDefault();
            const $sideMenu = document.querySelector('.side-menu');
            $sideMenu.classList.toggle('mobile-visible');
        };
    }
}

export default General;