class QuestionTypeController {

    constructor(){
        this.fieldClass = `form-control`;
    }

    generateField(question) {
        let htmlReturn = '';
        switch(question.question_type.name){
            case 'select':
                htmlReturn = this._GenerateSelect({id: question.id, options: question.options});
                break;
            case 'check':
                htmlReturn = this._GenerateCheckboxRadio({id: question.id, options: question.options, type: `checkbox`});
                break;
            case 'radio':
                htmlReturn = this._GenerateCheckboxRadio({id: question.id, options: question.options, type: `radio`});
                break;
            case 'text':
                htmlReturn = this._GenerateInput({id: question.id, type: `text`});
                break;
            case 'date':
                htmlReturn = this._GenerateInput({id: question.id, type: `date`});
                break;
            case 'mail':
                htmlReturn = this._GenerateInput({id: question.id, type: `mail`});
                break;
            case 'number':
                htmlReturn = this._GenerateInput({id: question.id, type: `number`});
                break;
            case 'textarea':
                htmlReturn = this._GenerateTextarea({id: question.id});
                break;
            case 'break':
                htmlReturn = this._GenerateBreak();
                break;
        }

        return htmlReturn;
    }

    _GenerateClass(){
        return `class="${this.fieldClass}"`;
    }

    _GenerateId(id, opcId = null, attr = 'id'){
        return opcId ? `${attr}="${id}_${opcId}"` : `${attr}="${id}"`;
    }

    _GenerateInput({id, type}) {
        return `<input type="${type}" ${this._GenerateClass()} ${this._GenerateId(id)}>`;
    }

    _GenerateSelect({id, options}) {
        return `
            <select ${this._GenerateClass()} ${this._GenerateId(id)}>
                ${Object.keys(options).map( item => {
                    return `<option value="${item}">${options[item]}</option>`;
                }).join('')}
            </select>
        `;
    }


    _GenerateCheckboxRadio({id, options, type}) {
        return `
            ${Object.keys(options).map( item => {
                return `
                    <div class="custom-control custom-${type}">
                        <input type="${type}" class="custom-control-input" ${this._GenerateId(id, item)} value="${item}">
                        <label class="custom-control-label" ${this._GenerateId(id, item, `for`)}>${options[item]}</label>
                    </div>
                `;
            }).join('')}
        `;
    }

    _GenerateTextarea({id}) {
        return `<textarea ${this._GenerateClass()} rows="5" ${this._GenerateId(id)}></textarea>`;
    }

    _GenerateBreak() {
        return `<hr>`;
    }


}

export default QuestionTypeController;