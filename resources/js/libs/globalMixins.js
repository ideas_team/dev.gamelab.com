import UserData from '../libs/userData';
const userData = new UserData;

export const computed = {
    accessAdmin() {
        return userData.user.role === 'admin';
    },
    accessEditor() {
        return userData.user.role === 'admin' || userData.user.role === 'editor';
    },
    accessCompany() {
        return userData.user.role === 'admin' || userData.user.role === 'editor' || userData.user.role === 'company';
    },
    accessCompanyWorker() {
        return userData.user.role === 'admin' || userData.user.role === 'editor' || userData.user.role === 'company' || userData.user.role === 'user_company';
    },
    userRole() {
        return userData.user.role;
    },
};

export default {
    computed,
};
