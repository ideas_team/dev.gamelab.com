<?php

use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->state(User::class, 'company', function(Faker $faker) {
    return [
        'role'=> $faker->randomElement(['company', 'user_company']),
    ];
});

$factory->state(User::class, 'editor', function(Faker $faker) {
    return [
        'role'=> 'editor',
    ];
});

$factory->state(User::class, 'respondent', function(Faker $faker) {
    return [
        'role'=> 'respondent',
    ];
});

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});
