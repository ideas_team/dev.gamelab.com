<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Company::class, 50)->create()->each(function ($company) {
            $company->users()->saveMany(factory(App\User::class, rand(1,5))->states('company')->make(['company_id'=>$company->id]));
            $company->activations()->saveMany(factory(App\CompanyActivation::class, rand(3,20))->make(['company_id'=>$company->id]));
        });
    }
}
