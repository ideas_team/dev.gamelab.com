@extends('layouts.app')

@section('content')

<company-activations />

@endsection

@section('page_js')
<script>
    _URL.set('api.activations', '{{ route('activations.company.get') }}');
</script>
<script src="{{ mix('js/activation.js') }}"></script>
@endsection