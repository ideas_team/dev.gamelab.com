<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivationAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activation_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('activation_question_id');
            $table->unsignedBigInteger('user_id');
            $table->text('answer');
            $table->boolean('is_multiple')->default(false);
            $table->timestamps();
            
            $table->foreign('activation_question_id')->references('id')->on('activation_questions');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activation_answers');
    }
}
