let instance = null;
class UserData {

    constructor(){
        if(!instance){
            instance = this;
        }
        //set all static data
        this._user = _USER;
        this._user_permissions = _USER_PERMISSION;
        return instance;
    }

    can(action){
        return !!this._user_permissions[_.camelCase(`can-${action}`)];
    }

    get user(){
        return this._user;
    }

}

export default UserData;