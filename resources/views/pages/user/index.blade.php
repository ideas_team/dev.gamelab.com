@extends('layouts.app')

@section('content')

<users />

@endsection

@section('page_js')
<script>
    _URL.set('users.get', '{{ route('users.get') }}');
    _URL.set('companies.get', '{{ route('companies.get') }}');
    _URL.set('user.store', '{{ route('user.store') }}');
    _URL.set('user.destroy', '{{ route('user.destroy', ['!user_id!' => '!user_id!']) }}');
    _URL.set('user.update', '{{ route('user.update', ['!user_id!' => '!user_id!']) }}');
    _URL.set('roles.get', '{{ route('roles.get') }}');
</script>
<script src="{{ mix('js/user.js') }}"></script>
@endsection