<?php

return [
    'email' => 'E-Mail Address',
    'forgot_pass' => 'Forgot Your Password?',
    'pass' => 'Password',
    'remember' => 'Remember Me',
    'login' => 'Login',
    'remember_btn' => 'Send password reset link',
];