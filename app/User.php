<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at',
    ];

    public function activations()
    {
        return $this->hasMany('App\CompanyActivation');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function accessAdmin()
    {
        return $this->role === 'admin';
    }

    public function accessEditor()
    {
        return $this->role === 'admin' || $this->role === 'editor';
    }

    public function accessCompany() {
        return $this->role === 'admin' || $this->role === 'editor' || $this->role === 'company';
    }

    public function accessCompanyWorker() {
        return $this->role === 'admin' || $this->role === 'editor' || $this->role === 'company' || $this->role === 'user_company';
    }

    public function avatar(){
        $hash = md5(strtolower($this->email));
        $fallback = urlencode("https://ui-avatars.com/api/".$this->name."/128");
        return "https://www.gravatar.com/avatar/".$hash."?d=".$fallback;
    }

    public function userPermissions(){

        $permissions = [
            'canAddCompany' => true,
        ];

        return json_encode($permissions);
    }

    public function accessRoles()
    {
        $rolesByRole = [
            'admin' => [
                'admin' => 'Administrador',
                'editor' => 'Editor',
                'company' => 'Empresa',
                'user_company' => 'Usuario',
            ],
            'editor' => [
                'company' => 'Empresa',
                'user_company' => 'Usuario',
            ],
            'company' => [
                'user_company' => 'Usuario',
            ],
        ];

        return isset($rolesByRole[$this->role]) ? $rolesByRole[$this->role] : null;
    }
}
