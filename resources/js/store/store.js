import Vue from "vue";
import Vuex from "vuex";

import companies from "./modules/companies";
import toasts from "./modules/toasts";
import roles from "./modules/roles";
import activationSections from "./modules/activationSections";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: [
    companies,
    toasts,
    roles,
    activationSections,
  ]
});
