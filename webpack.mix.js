const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.options({
    processCssUrls: false // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
});
//compile js
mix.js('resources/js/pages/home.js', 'public/js')
    .js('resources/js/pages/questionary.js', 'public/js')
    .js('resources/js/pages/user.js', 'public/js');

//copy folders
mix.copyDirectory('resources/images', 'public/images')
    .copyDirectory('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/webfonts');

//compilig styles
mix.sass('resources/sass/theme.scss', 'public/css')
    .sass('resources/sass/dev.scss', 'public/css');

//Other configurations
mix.extract(['vue', 'axios', 'jquery', 'moment'])
    .version()
    .sourceMaps()
    .browserSync('dev.gamelab.com');
