const companies = {
    state: {
        companies: [],
    },
    getters: {
        companies(state) {
          return state.companies;
        },
    },
    mutations: {
        setCompanies(state, companyList) {
          state.companies = companyList;
        },
    },
    actions: {
        setCompanies: ({commit, state}, {companyList}) => {
            commit('setCompanies', {companyList});
            return state.companies;
        },
    },
};
export default companies;