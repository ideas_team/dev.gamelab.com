<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivationSection extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = [
        'company_activation_id',
        'title',
        'subtitle',
        'description',
        'page',
    ];

    public function questions()
    {
        return $this->hasMany('App\ActivationQuestion');
    }

    public function activation()
    {
        return $this->belongsTo('App\CompanyActivation');
    }
}
