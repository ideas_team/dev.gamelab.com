export function QueryStringFromObject(obj) {
    return Object.keys(obj)
        .map(k => obj[k] !== '' ? `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}` : '')
        .join('&');
}

export function HighlightSearch(original, searchTerm) {
    if (searchTerm) {
        const reg = new RegExp(`(${searchTerm})`, 'gi');
        return original.replace(reg, `<mark>$1</mark>`);
    }

    return original;
}

export function ValidateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

export function GeneratePassword (length) {
	var password = '', character; 
	while (length > password.length) {
		if (password.indexOf(character = String.fromCharCode(Math.floor(Math.random() * 94) + 33), Math.floor(password.length / 94) * 94) < 0) {
			password += character;
		}
	}
	return password;
}

export function GetCompanyName(companyList, companyId){
    const company = companyList.find( item => item.id === companyId);
    return typeof company !== 'undefined' ? company.name : '';
}

export function TimeForHumans(time){
    const moment = require('moment');
    return moment(time, 'YYYY-MM-DD hh:mm:ss').fromNow();
};

export function FormatDate(date, format = 'DD/MM/YYYY'){
    const moment = require('moment');
    return moment(date).format(format);
};

export function CleanContentEditable(value) {
    return value
            .trim()
            .replace(/\n/g, '');
};

export default {
    QueryStringFromObject,
    HighlightSearch,
    ValidateEmail,
    GeneratePassword,
    GetCompanyName,
    TimeForHumans,
    FormatDate,
    CleanContentEditable,
};
