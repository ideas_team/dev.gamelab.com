const toasts = {
    state: {
        toasts: [],
    },
    getters: {
        toasts(state) {
          return state.toasts;
        },
        lastToast(state) {
            return state.toasts[0];
        },
    },
    mutations: {
        setToast(state, toast) {
            state.toasts.unshift(toast);
        },
    },
    actions: {
        setToast: ({commit, state}, toast) => {
            commit('setToast', toast);
            return state.toasts;
        },
    },
};
export default toasts;