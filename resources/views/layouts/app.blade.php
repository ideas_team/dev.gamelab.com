@include('includes.assets')
@include('includes.tracking')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    @yield('assets_header')
    @yield('page_header')
</head>

<body>
    <div id="app">
        @auth
        @include('modules.header')
        @include('modules.left_sidebar')
        @include('modules.content')
        @endauth
        @guest
        @yield('content')
        @endguest
    </div>
</body>

</html>