<?php

use Illuminate\Database\Seeder;

use App\QuestionType;

class QuestionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questionTypes = [
            [
                'name' => 'text', 
                'css_class' => 'fas fa-font',
            ],[
                'name' => 'select', 
                'css_class' => 'fas fa-stream',
            ],[
                'name' => 'check', 
                'css_class' => 'fas fa-check-square',
            ],[
                'name' => 'radio', 
                'css_class' => 'fas fa-check-circle',
            ],[
                'name' => 'date', 
                'css_class' => 'fas fa-calendar-alt',
            ],[
                'name' => 'mail', 
                'css_class' => 'fas fa-at',
            ],[
                'name' => 'number', 
                'css_class' => 'fas fa-hashtag',
            ],[
                'name' => 'textarea', 
                'css_class' => 'fas fa-file-alt',
            ],[
                'name' => 'break', 
                'css_class' => 'fas fa-grip-lines',
            ],[
                'name' => 'section', 
                'css_class' => 'fas fa-clone',
            ],
        ];

        foreach($questionTypes as $type){
            QuestionType::create($type);
        }
    }
}
