<!-- Top Bar Start -->
<div class="topbar">

    <!-- LOGO -->
    <div class="topbar-left">
        <!-- Image logo -->
        <a href="" class="logo">
            <span>
                <img src="{{ asset('images/misc/logo-atelier.png') }}" alt="">
            </span>
        </a>
    </div>

    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar navbar-default" role="navigation">
        <div class="container-fluid">

            <!-- Navbar-left -->
            <ul class="nav navbar-left">

            </ul>

            <!-- Right(Notification) -->
            <ul class="nav navbar-right list-inline">
                {{-- <li class="d-none d-sm-block list-inline-item">
                    <form role="search" class="app-search">
                        <input type="text" placeholder="Search..." class="form-control">
                        <a href=""><i class="fa fa-search"></i></a>
                    </form>
                </li> --}}
                <li class="list-inline-item">
                    <div class="dropdown">
                        <a href="#" class="right-menu-item dropdown-toggle" data-toggle="dropdown">
                            <i class="fas fa-bell"></i>
                            <span class="badge badge-pill badge-pink">4</span>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right dropdown-lg user-list notify-list">
                            <li class="list-group notification-list m-b-0">
                                <div class="slimscroll">
                                    <!-- list item-->
                                    <a href="javascript:void(0);" class="list-group-item">
                                        <div class="media">
                                            <div class="media-left p-r-10">
                                                <em class="fa fa-diamond bg-primary"></em>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">A new order has been placed A new order has
                                                    been placed</h5>
                                                <p class="m-0">
                                                    There are new settings available
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </li>
                            <!-- end notification list -->
                        </ul>
                    </div>
                </li>

                <li class="dropdown user-box list-inline-item">
                    <a href="" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
                        <img
                            src="{{ Auth::user()->avatar() }}"
                            alt="user-img" class="rounded-circle user-img">
                    </a>

                    <ul
                        class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                        <li><a href="javascript:void(0)" class="dropdown-item">{{ Auth::user()->name }}</a></li>
                        <li class="dropdown-divider"></li>
                        <li><a href="javascript:void(0)" class="dropdown-item">Profile</a></li>
                        <li><a href="javascript:void(0)" class="dropdown-item">Settings</a></li>
                        <li class="dropdown-divider"></li>
                        <li><a href="{{ route('logout') }}" class="dropdown-item">Logout</a></li>
                    </ul>
                </li>

                <li class="list-inline-item mobile-menu d-xl-none d-lg-none">
                    <a href="#" class="waves-effect user-link" id="mobile-menu-button">
                        <i class="fas fa-bars fa-2x"></i>
                    </a>
                </li>

            </ul> <!-- end navbar-right -->

        </div><!-- end container-fluid -->
    </div><!-- end navbar -->
</div>
<!-- Top Bar End -->
