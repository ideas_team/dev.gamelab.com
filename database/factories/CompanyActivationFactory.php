<?php

use Faker\Generator as Faker;

$factory->define(App\CompanyActivation::class, function (Faker $faker) {

    $startTime = $faker->dateTimeBetween('-8 months', '+ 1 month');
    $endTime = $faker->dateTimeBetween($startTime, '+ 4 months');

    return [
        'name' => $faker->sentence(3, true),
        'detail' => $faker->paragraph(3, true),
        'force_close' => $faker->randomElement([0,0,0,0,0,1]),
        'starts_at' => $startTime,
        'ends_at' => $endTime,
        'user_id' => 1,
    ];
});
