@extends('layouts.app')

@section('content')

<!-- HOME -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <div class="wrapper-page">

                    <div class="m-t-40 account-pages">
                        <div class="text-center account-logo-box">
                            <h2 class="text-uppercase">
                                <a href="index.html" class="text-success">
                                    <span><img src="{{ asset('images/misc/logo-atelier.png') }}" alt=""></span>
                                </a>
                            </h2>
                            <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                        </div>
                        <div class="account-content">
                            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group m-b-25">
                                    <div class="col-12">
                                        <label for="emailaddress">{{ __('form.email') }}</label>
                                        <input id="email" type="email"
                                            class="form-control input-lg{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                            value="{{ old('email') }}" required autofocus>
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group m-b-25">
                                    <div class="col-12">
                                        @if (Route::has('password.request'))
                                        <a class="text-muted float-right" href="{{ route('password.request') }}">
                                            {{ __('form.forgot_pass') }}
                                        </a>
                                        @endif
                                        <label for="password">{{ __('form.pass') }}</label>
                                        <input id="password" type="password"
                                            class="form-control input-lg{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                            name="password" required>

                                        @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group m-b-20">
                                    <div class="col-12">

                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" name="remember" id="remember"
                                                {{ old('remember') ? 'checked' : '' }}>

                                            <label class="custom-control-label" for="remember">
                                                {{ __('form.remember') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>



                                <div class="form-group account-btn text-center m-t-10">
                                    <div class="col-12">
                                        <button class="btn w-lg btn-rounded btn-lg btn-primary waves-effect waves-light"
                                            type="submit">
                                            {{ __('form.login') }}
                                        </button>
                                    </div>
                                </div>

                            </form>

                            <div class="clearfix"></div>

                        </div>
                    </div>
                    <!-- end card-box-->

                </div>
                <!-- end wrapper -->

            </div>
        </div>
    </div>
</section>
<!-- END HOME -->
@endsection
