window.Vue = require('vue');
import General from '../libs/general';
import { computed } from '../libs/globalMixins';
import store from "../store/store";

Vue.mixin({ computed });
require('../bootstrap');

Vue.component('users', require('../components/user/List.vue').default);
Vue.component('user-modal', require('../components/user/Add.vue').default);
Vue.component('toast', require('../components/general/Toast.vue').default);

new Vue({
    el: '#app',
    store,
    mounted() {
        new General;
    }
});
