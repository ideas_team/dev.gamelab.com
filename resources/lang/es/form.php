<?php

return [
    'email' => 'E-Mail',
    'forgot_pass' => 'Olvidaste tu contraseña?',
    'pass' => 'Contraseña',
    'remember' => 'Recordarme',
    'login' => 'Login',
    'remember_btn' => 'Enviar link de recuperación de contraseña',
];