<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">

                <li>
                    <a href="{{ route('index') }}">
                        <i class="fas fa-chart-bar"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                @if(Auth::user()->accessEditor())
                <li class="menu-title">Editores</li>
                <li class="{{ (Request::is('activation'))?'mm-active':'' }}">
                    <a class="{{ (Request::is('activation'))?'mm-active':'' }}" href="{{ route('activation.index') }}">
                        <i class="fas fa-edit"></i>
                        <span>Activaciones</span>
                    </a>
                </li>
                @endif

                @if(Auth::user()->accessCompany())
                <li class="menu-title">Administrador</li>
                <li class="{{ (Request::is('user'))?'mm-active':'' }}">
                    <a href="{{ route('user.index') }}" class="{{ (Request::is('user'))?'mm-active':'' }}">
                        <i class="fas fa-users"></i>
                        <span>Usuarios</span>
                    </a>
                </li>
                {{-- @if(Auth::user()->accessEditor())
                <li class="{{ (Request::is('company'))?'mm-active':'' }}">
                    <a href="{{ route('user.index') }}" class="{{ (Request::is('company'))?'mm-active':'' }}">
                        <i class="fas fa-building"></i>
                        <span>Empresas</span>
                    </a>
                </li>
                @endif --}}
                @endif

                {{-- @if(Auth::user()->role(['company', 'user_company']))
                <li class="menu-title">Usuario</li>
                <li>
                    <a href="{{ route('index') }}">
                <i class="fas fa-chart-bar"></i>
                <span>Crear Activación</span>
                </a>
                </li>
                @endif --}}

                @if(Auth::user()->accessAdmin())
                <li class="menu-title">Root</li>
                <li>
                    <a href="{{ route('index') }}">
                        <i class="fas fa-users"></i>
                        <span>Usuarios Globales</span>
                    </a>
                </li>
                @endif

            </ul>

        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->