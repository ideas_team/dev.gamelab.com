const roles = {
    state: {
        roles: {},
    },
    getters: {
        roles(state) {
          return state.roles;
        },
    },
    mutations: {
        setRoles(state, {rolesList}) {
          state.roles = rolesList;
        },
    },
    actions: {
        setRoles: ({commit, state}, {rolesList}) => {
            commit('setRoles', {rolesList});
            return state.roles;
        },
    },
};
export default roles;