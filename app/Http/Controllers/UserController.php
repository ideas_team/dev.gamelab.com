<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Validator;
use Auth;
use App;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.user.index');
    }
    
    /**
     * Return Users depending on what the loged user can see
     * 
     * @return \Illuminate\Http\Response
     */
    public function GetUsers(Request $request){
        $user = Auth::user();

        $userData = User::with(['company'])
                            ->whereNotIn('id', [1])
                            ->whereNotIn('role', ['respondent']);

        if($user->accessAdmin()){
            $userData->withTrashed();
        }

        if($user->company_id !== null){
            $userData->where('company_id', $user->company_id);
        }
        
        
        if($request->has('name')){
            $name = strtolower($request->get('name'));
            $userData->where(function($query) use($name){
                $query->whereRaw('LOWER(`name`) like ?', ['%'.$name.'%']);
            });
        }
        
        if($request->has('company')){
            $userData->where('company_id', $request->get('company'));
        }

        $users = $userData->paginate();

        if(App::environment('local')){
            $users->withPath('http://localhost:3000/users-get');
        }

        return compact('users');
    }

    /**
     * Return Users roles depending on what the loged user can see
     * 
     * @return \Illuminate\Http\Response
     */
    public function GetRoles(Request $request)
    {
        $user = Auth::user();
        $roles = $user->accessRoles();
        return compact('roles');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8',
            'role' => 'nullable',
            'company' => 'nullable|integer',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return ['success'=>false, 'error'=>$validator->errors()];
        } else {
            $user = new User;
            $user->company_id = $request->input('company');
            $user->name = $request->input('name');
            $user->role = $request->input('role');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));
            $user->save();

            $success = true;
            return compact('success', 'user');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8',
            'role' => 'nullable',
            'company' => 'nullable|integer',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return ['success'=>false, 'error'=>$validator->errors()];
        } else {
            $user = User::find($id);
            $user->company_id = $request->input('company');
            $user->name = $request->input('name');
            $user->role = $request->input('role');
            $user->email = $request->input('email');
            if($request->input('password') !== '--------'){
                $user->password = bcrypt($request->input('password'));
            }
            $user->save();

            $success = true;
            return compact('success', 'user');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::with('company')->withTrashed()->find($id);
        if($user->deleted_at === null){
            $user->delete();
        } else {
            $user->restore();
        }

        return $user;
    }
}
