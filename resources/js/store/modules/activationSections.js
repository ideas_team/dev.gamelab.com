const activationSections = {
    state: {
        activationSections: {},
    },
    getters: {
        activationSections(state) {
            return state.activationSections;
        },
    },
    mutations: {
        setActivationSections(state, { activationSections }) {
            state.activationSections = activationSections;
        },

        addNewSection(state, { company_activation_id, description, id, page, subtitle, title }) {
            state.activationSections.sections.push({ company_activation_id, description, id, page, subtitle, title });
        },

        editSectionAttribute(state, {index, attribute, value = null}){
            if(typeof state.activationSections.sections[index] !== `undefined`){
                state.activationSections.sections[index][attribute] = value;
            }
        },
    },
    actions: {
        setActivationSections: ({commit, state}, { activationSections }) => {
            commit('setActivationSections', { activationSections });
            return state.activationSections;
        },

        addNewSection: ({commit, state}, {
            company_activation_id, 
            description = 'Esta es la descripción por defecto',
            id = -1,
            page = 1,
            subtitle = 'Este es el subtítulo por defecto',
            title = 'Título'
        }) => {
            commit('addNewSection', { company_activation_id, description, id, page, subtitle, title });
            return state.activationSections
        },

        editSectionAttribute({commit, state}, {index, attribute, value}) {
            commit('editSectionAttribute', {index, attribute, value});
            return state.activationSections;
        },
        
        deleteSectionAttribute({commit, state}, {index, attribute}) {
            commit('editSectionAttribute', {index, attribute})
            return state.activationSections;
        },
    },
};
export default activationSections;