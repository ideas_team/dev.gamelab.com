import General from '../libs/general';
import store from "../store/store";
require('../bootstrap');

window.Vue = require('vue');

Vue.component('toast', require('../components/general/Toast.vue').default);

const app = new Vue({
    el: '#app',
    store,
    mounted() {
        new General;
    }
});
