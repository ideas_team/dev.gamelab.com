<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivationQuestion extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = [
        'activation_section_id','question_type_id','question','options','order','caption','required','restrictions'
    ];
    protected $casts = [
        'options' => 'array',
        'restrictions' => 'array',
    ];

    public function answers()
    {
        return $this->hasMany('App\ActivationAnswer');
    }

    public function section()
    {
        return $this->belongsTo('App\ActivationSection');
    }

    public function questionType()
    {
        return $this->belongsTo('App\QuestionType');
    }
}
