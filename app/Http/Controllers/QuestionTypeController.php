<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\QuestionType;

class QuestionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return QuestionType::all();
    }
}
