<?php

use Illuminate\Database\Seeder;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Daniel Flórez',
                'role' => 'admin',
                'email' => 'daniel.florez.murillo@gmail.com',
                'password' => bcrypt('RX?[5qB9,bJU@pfC~m"d'),
            ],
            [
                'name' => 'Nicolas Saiz',
                'role' => 'admin',
                'email' => 'nicolas.saiz@atelier-consulting.com',
                'password' => bcrypt('RX?[5qB9,bJU@pfC~m"d'),
            ],
        ];

        foreach($users as $user){
            User::create($user);
        }

        factory(App\User::class, 3)->states('editor')->create();
        factory(App\User::class, 50)->states('respondent')->create();
    }
}
