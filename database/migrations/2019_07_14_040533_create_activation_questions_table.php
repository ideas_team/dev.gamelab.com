<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivationQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activation_questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('activation_section_id');
            $table->unsignedBigInteger('question_type_id');
            $table->string('question')->nullable();
            $table->json('options')->nullable();
            $table->json('restrictions')->nullable();
            $table->integer('order')->default(1);
            $table->text('caption')->nullable();
            $table->boolean('required')->default(true);
            $table->timestamps();
            
            $table->foreign('activation_section_id')->references('id')->on('activation_sections');
            $table->foreign('question_type_id')->references('id')->on('question_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activation_questions');
    }
}
