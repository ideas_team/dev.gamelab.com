<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyActivation extends Model
{

    protected $hidden = ['created_at', 'updated_at'];

    public function sections()
    {
        return $this->hasMany('App\ActivationSection');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
