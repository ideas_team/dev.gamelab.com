window.Vue = require('vue');
import General from '../libs/general';
require('../bootstrap');

Vue.component('company-activations', require('../components/CompanyActivationsComponent.vue').default);

new Vue({
    el: '#app',
    mounted() {
        new General;
    }
});
