@section('assets_header')
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if(env('APP_ENV') != 'production')
        <link rel="stylesheet" href="{{ mix('/css/dev.css') }}">
    @endif

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ mix('css/theme.css') }}" rel="stylesheet">

    <script src="https://cdn.polyfill.io/v2/polyfill.min.js" defer></script>

    <script type="text/javascript">
        window._URL = (function(){var _url = {};var SetUrl = function(key, value){_url[key] = value;};var GetUrl = function(key, params){var params = params || [];var retUrl = _url[key];for (var key in params) {if (params.hasOwnProperty(key)) {var element = params[key];var reg = new RegExp('!'+key+'!');retUrl = retUrl.replace(reg, element);}}return retUrl;};return {set: SetUrl,get: GetUrl}})();
        _URL.set('app', '{{ url('/') }}');
        @auth
        var _USER = {!! Auth::user() !!};
        var _USER_PERMISSION = {!! Auth::user()->userPermissions() !!};
        @endauth
    </script>

    <script src="{{ mix('/js/manifest.js') }}" defer></script>
    <script src="{{ mix('/js/vendor.js') }}" defer></script>

    @yield('page_js')

@endsection
